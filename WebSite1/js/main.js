﻿/* Created at 10/28/2018 */

var xmlHttpObject = false;

if (typeof XMLHttpRequest != 'undefined') {
    xmlHttpObject = new XMLHttpRequest();
}

if (!xmlHttpObject) {
    try {
        xmlHttpObject = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e) {
        try {
            xmlHttpObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (e) {
            xmlHttpObject = null;
        }
    }
}

function handleContent() {
    if (xmlHttpObject.readyState == 4) {
        document.getElementById('Content').innerHTML = xmlHttpObject.responseText;
    }
}

/* Lade Website Dynamisch */

function LoadHome() {
    document.getElementById('BoxTitle').innerHTML = "Home";
    xmlHttpObject.open('get', './content/default.html');
    xmlHttpObject.onreadystatechange = handleContent;
    xmlHttpObject.send(null);
    return false;
}

function LoadRelease() {
    document.getElementById('BoxTitle').innerHTML = "Releases";
    xmlHttpObject.open('get', './content/releases.html');
    xmlHttpObject.onreadystatechange = handleContent;
    xmlHttpObject.send(null);
    return false;
}